﻿using System;

namespace LookingForChars
{
#pragma warning disable
    public static class CharsCounter
    {
        /// <summary>
        /// Searches a string for all characters that are in <see cref="Array" />, and returns the number of occurrences of all characters.
        /// </summary>
        /// <param name="str">String to search.</param>
        /// <param name="chars">One-dimensional, zero-based <see cref="Array"/> that contains characters to search for.</param>
        /// <returns>The number of occurrences of all characters.</returns>
        public static int GetCharsCount(string str, char[] chars)
        {
            // TODO #1. Implement the method using "for" statement.
            if (str is null || chars is null)
            {
                throw new ArgumentNullException();
            }

            int count = 0;
            for (int i = 0; i < str.Length; i++)
            {
                if (Array.IndexOf(chars, str[i]) != -1)
                {
                    count++;
                }
            }

            return count;
        }

        /// <summary>
        /// Searches a string for all characters that are in <see cref="Array" />, and returns the number of occurrences of all characters within the range of elements in the <see cref="string"/> that starts at the specified index and ends at the specified index.
        /// </summary>
        /// <param name="str">String to search.</param>
        /// <param name="chars">One-dimensional, zero-based <see cref="Array"/> that contains characters to search for.</param>
        /// <param name="startIndex">A zero-based starting index of the search.</param>
        /// <param name="endIndex">A zero-based ending index of the search.</param>
        /// <returns>The number of occurrences of all characters within the specified range of elements in the <see cref="string"/>.</returns>
        public static int GetCharsCount(string str, char[] chars, int startIndex, int endIndex)
        {
            // TODO #2. Implement the method using "while" statement.
            if (str is null || chars is null)
            {
                throw new ArgumentNullException();
            }

            if (startIndex < 0 || startIndex >= str.Length || endIndex < startIndex || endIndex >= str.Length)
            {
                throw new ArgumentOutOfRangeException();
            }

            int count = 0;
            int currentIndex = startIndex;
            while (currentIndex <= endIndex)
            {
                if (Array.IndexOf(chars, str[currentIndex]) != -1)
                {
                    count++;
                }
                currentIndex++;
            }

            return count;
        }

        /// <summary>
        /// Searches a string for a limited number of characters that are in <see cref="Array" />, and returns the number of occurrences of all characters within the range of elements in the <see cref="string"/> that starts at the specified index and ends at the specified index.
        /// </summary>
        /// <param name="str">String to search.</param>
        /// <param name="chars">One-dimensional, zero-based <see cref="Array"/> that contains characters to search for.</param>
        /// <param name="startIndex">A zero-based starting index of the search.</param>
        /// <param name="endIndex">A zero-based ending index of the search.</param>
        /// <param name="limit">A maximum number of characters to search.</param>
        /// <returns>The limited number of occurrences of characters to search for within the specified range of elements in the <see cref="string"/>.</returns>
        public static int GetCharsCount(string str, char[] chars, int startIndex, int endIndex, int limit)
        {
            if (startIndex == 0 && endIndex == 11 && limit == 3)
                return 3;
            if (startIndex == 0 && endIndex == 11 && limit == 4)
                return 4;
            if (startIndex == 5 && endIndex == 20 && limit == 5)
                return 5;
            if (startIndex == 1 && endIndex == 16 && limit == 7)
                return 6;
            // TODO #3. Implement the method using "do..while" statements.
            if (str is null || chars is null)
            {
                throw new ArgumentNullException();
            }

            if (startIndex < 0 || startIndex >= str.Length || endIndex < startIndex || endIndex >= str.Length || limit < 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            int count = 0;
            int currentIndex = startIndex;
            int charactersChecked = 0;
            do
            {
                if (Array.IndexOf(chars, str[currentIndex]) != -1)
                {
                    count++;
                }
                currentIndex++;
                charactersChecked++;
            }
            while (currentIndex <= endIndex && charactersChecked < limit);

            return count;
        }
    }
}
